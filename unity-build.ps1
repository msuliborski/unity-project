param(
    [Parameter(Mandatory=$true)][string]$projectFolder
)

Write-Host "================== Building Standalone64 ==================`n`n"

Write-Host "Parameters: "
Write-Host "Project folder: $projectFolder"

$unityDir = "C:\Program Files\Unity\Hub\Editor"
$unityVersion = "2019.4.33f1"

$currentDate = (Get-Date).ToUniversalTime().ToString("yyyy-MM-dd_HH-mm-ss")
$logFile = "$projectFolder\unity-win-build_$currentDate.log" 
if (Test-Path "$logFile") { Remove-Item $logFile }

Write-Host "Building..."
$process = (Start-Process -FilePath "$unityDir\$unityVersion\Editor\Unity.exe" -ArgumentList '-batchmode', '-quit', '-nographics', '-logfile', "$logFile", '-projectPath', "$projectFolder", '-buildTarget Standalone', '-executeMethod Build.Standalone64Build' -NoNewWindow -PassThru)
do { Start-Sleep -Milliseconds 500 } until ($process.HasExited)


if ($process.ExitCode -eq $null -Or $process.ExitCode -eq "") {
    Write-Host "Process finished, but without an exit code. Check the logs!"
} elseif ( $process.ExitCode -ne 0 ) {
    throw "An error occured during the build."
} else {
    Write-Host "Build successful!"
}

if (!(Test-Path "$projectFolder\Builds/Standalone64/UnityProject.exee")) {
    throw "Building Standalone64 version finished, but no .exe file detected! Check the logs!"
}
