using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System;
using System.IO;
using System.Globalization;


public static class Build
{
    [MenuItem("Build/Build Standalone64")]
    public static void Standalone64Build()
    { 
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
        EditorUserBuildSettings.development = false;
        EditorUserBuildSettings.allowDebugging = true;

        List<string> scenes = new List<string>();
        scenes.Add("Assets/Scenes/SampleScene.unity");

        BuildPipeline.BuildPlayer(scenes.ToArray(), "Builds/Standalone64/UnityProject.exe", BuildTarget.StandaloneWindows64, BuildOptions.None);
    }

    private static string[] GetCommandLineArgs()
    {
        var args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            Console.WriteLine("[Build][GetCommandLineArgs][" + i + ": " + args[i] + "]");
        }
        return System.Environment.GetCommandLineArgs();
    }
}
